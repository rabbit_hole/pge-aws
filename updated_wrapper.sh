#!/bin/bash
current_directory=`pwd`

check_existing_certificate=`aws iam list-server-certificates`

echo "Checking for pre-existing certificate"

if echo "$check_existing_certificate" | grep -q "pgedirectorycertificate"; then
  echo "Certificate found to be pre-existing, removing and uploading a new one";
  aws iam delete-server-certificate --server-certificate-name pgedirectorycertificate
  aws iam upload-server-certificate --server-certificate-name pgedirectorycertificate --certificate-body file:///$current_directory/ca.pem --private-key file:///$current_directory/ca.key

else
  echo "Certificate not found, uploading a new one";
  aws iam upload-server-certificate --server-certificate-name pgedirectorycertificate --certificate-body file:///$current_directory/ca.pem --private-key file:///$current_directory/ca.key
fi

#Creating bucket for storing the files
aws s3 rb s3://my-bucket-0909090 --force
sleep 5
aws s3api create-bucket --bucket my-bucket-0909090 --region us-east-1
sleep 5
aws s3api put-object --bucket my-bucket-0909090 --key PhoneBookWebApp.war --body $current_directory/PhoneBookWebApp.war
sleep 2
aws s3api put-object --bucket my-bucket-0909090 --key employees.json --body $current_directory/employees.json
sleep 2
#aws s3api put-bucket-cors --bucket my-bucket-0909090 --cors-configuration file:///$current_directory/bucket_cors.json
#sleep 2
#aws s3api put-bucket-policy --bucket my-bucket-0909090 --policy file:///$current_directory/bucket_policy.json

aws cloudformation create-stack --template-body file:///$current_directory/updated_cloud_formation.json --capabilities CAPABILITY_NAMED_IAM --stack-name pge-directory --parameters ParameterKey=KeyName,ParameterValue=new_office ParameterKey=UserDataFile,ParameterValue=$(base64 -w0 $current_directory/updated_configure.sh)

z=`aws cloudformation wait stack-create-complete --stack-name pge-directory`
ip_array=()
for i in `aws ec2 describe-instances --filters "Name=instance-state-code,Values=16"  |  grep -E 'InstanceId|PublicIpAddress'`
do
  if [[ $i = *"i-"* ]]
  then
    instance_id=`echo $i | cut -f1 -d ","`
    echo "Instance id of the instance created is $instance_id"
  elif [[ $i = *[0-9]* ]]
  then
    public_ip=`echo $i | cut -f2 -d '"'|cut -f1 -d '"'`
    echo "Public ip of instance created is $public_ip"
    ip_array+=($public_ip)
  fi
done

for public_ip in ${ip_array[@]}; do
   echo "Waiting for tomcat to be available on $public_ip"
   nc -vzw 1 $public_ip 8080 2> /dev/null
   while [ "$?" -eq 1 ]
   do
      sleep 10
      nc -vzw 1 $public_ip 8080 2> /dev/null
   done
done
sleep 60
echo "Tomcat is available on all instances now"
