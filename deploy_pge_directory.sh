#!/usr/bin/bash
echo "Deploying Serverless web service using Lambda and Zappa"

echo "Chekcking for dependencies"
type flask >/dev/null 2>&1 || { echo >&2 "Script needs flask to be installed but it's not installed.  Aborting."; exit 1;}
type zappa >/dev/null 2>&1 || { echo >&2 "Script needs zappa to be installed but it's not installed.  Aborting."; exit 1;}
type jar > /dev/null 2>&1 || { echo >&2 "Script needs java-development (jar) to be installed but it's not installed.  Aborting."; exit 1;}
type aws > /dev/null 2>&1 || { echo >&2 "Script needs aws-cli to be installed but it's not installed.  Aborting."; exit 1;}

echo "All dependencies have been met, continuing with deployent"

deploy_directory=`pwd`
parentdir="$(dirname "$deploy_directory")"
cd $parentdir
echo "parent_directory is $parentdir"
lambda_dir="aws-lambda"
lambda_service_dir=$parentdir/$lambda_dir
if [[ -d "$lambda_service_dir" ]]; then
    cd $lambda_service_dir
    #git pull
    cd $deploy_directory
else
    cd $parentdir
    #git clone git@bitbucket.org:rabbit_hole/aws-lambda.git
    cd $deploy_directory
fi

authorizer_dir="authorizer"
authorizer_service_dir=$parentdir/$authorizer_dir

if [[ -d "$authorizer_service_dir" ]]; then
    cd $authorizer_service_dir
    #git pull
    cd $deploy_directory
else
    cd $parentdir
    #git clone git@bitbucket.org:rabbit_hole/authorizer.git
    cd $deploy_directory
fi

current_directory=`pwd`
origin_directory=$current_directory

cd $authorizer_service_dir
echo "Deploying Lambda based authorizer service"
x=$((zappa undeploy -y test --remove-logs) 2>&1)
y=$((zappa deploy test) 2>&1)

cd $lambda_service_dir
current_directory=`pwd`
echo "Deploying Lambda based backend Restful Webservice"
x=$((zappa undeploy -y test --remove-logs) 2>&1)
y=$((zappa deploy test) 2>&1)

new_url=`echo $y | awk '{print $NF}'`

#Securing Lambda
echo "Securing Lambda backend with a lambda authorizer"
rest_api_id=`echo $new_url | awk -F/ '{print $3}' | cut -f1 -d "."`

authorizer_credentials=`aws iam get-role --role-name aws-lambda-authorizer-test-ZappaLambdaExecutionRole | python -c "import sys, json; \
x=json.load(sys.stdin)['Role'];authorizer_credentials=x['Arn'];print(authorizer_credentials)"`

authorizer_lambda_arn=`aws lambda get-function --function-name aws-lambda-authorizer-test | python -c "import sys,json; \
x=json.load(sys.stdin)['Configuration']['FunctionArn']; print(x)"`

authorizer_uri="arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/"
ending="invocations"
authorizer_uri=$authorizer_uri$authorizer_lambda_arn"/"$ending

aws apigateway create-authorizer --rest-api-id $rest_api_id --name python_lambda_authorizer \
--type TOKEN --identity-source method.request.header.Authorization \
--identity-source 'method.request.header.Authorization' --identity-validation-expression '^Bearer [-0-9a-zA-z\\.]*$' \
--authorizer-credentials $authorizer_credentials \
--authorizer-uri $authorizer_uri \
--authorizer-result-ttl-in-seconds 300

authorizer_id=`aws apigateway get-authorizers --rest-api-id $rest_api_id | python -c "import sys, json; x=json.load(sys.stdin)['items'];y = [ i for i in x if 'python_lambda_authorizer' == i['name']];authorizer_id=y[0]['id']; print(authorizer_id)"`

root_resource_id=`aws apigateway get-resources --rest-api-id $rest_api_id | python -c "import sys, json; x=json.load(sys.stdin)['items'];\
y = [ i for i in x if 'proxy' not in i['path']]; resource_id=y[0]['id']; print(resource_id)"`

proxy_resource_id=`aws apigateway get-resources --rest-api-id $rest_api_id | python -c "import sys, json; x=json.load(sys.stdin)['items'];\
y = [ i for i in x if 'proxy' in i['path']]; resource_id=y[0]['id']; print(resource_id)"`

aws apigateway update-method --rest-api-id $rest_api_id --resource-id $root_resource_id --http-method ANY --patch-operations "op=replace,path=/authorizationType,value=CUSTOM" "op=replace,path=/authorizerId,value=$authorizer_id"

aws apigateway update-method --rest-api-id $rest_api_id --resource-id $proxy_resource_id --http-method ANY --patch-operations "op=replace,path=/authorizationType,value=CUSTOM" "op=replace,path=/authorizerId,value=$authorizer_id"

aws apigateway create-deployment --rest-api-id $rest_api_id --stage-name test --stage-description 'Test Stage' --description 'Deploy post authorization'

echo "New backend service URL is $new_url"
context="/phonebookservice/employeeBook/employees"
new_url=$new_url$context

echo "Updating UI application with new backend service URL"
cd $deploy_directory
echo "Deploy Directory is $deploy_directory"
current_directory=`pwd`
rm -rf $deploy_directory/temp
mkdir $deploy_directory/temp
cp PhoneBookWebApp.war $deploy_directory/temp
cd $deploy_directory/temp
current_directory=`pwd`

jar -xvf PhoneBookWebApp.war &> /dev/null
rm PhoneBookWebApp.war
main_file=`ls main* | grep js`
old_url=`grep -o '"https://[^"]*"' $main_file | grep phonebookservice`
old_url=`echo $old_url | cut -f2 -d '"'`
sed -i "s,$old_url,$new_url,g" $main_file
changed_url=`grep -o '"https://[^"]*"' $main_file`

#Deploying token getter service
token_generator_dir="token_generator"
token_generator_service_dir=$parentdir/$token_generator_dir
cd $token_generator_service_dir
echo "Deploying Lambda based token generator Webservice"
x=$((zappa undeploy -y test --remove-logs) 2>&1)
y=$((zappa deploy test) 2>&1)
new_url=`echo $y | awk '{print $NF}'`

#Getting old token generator url
cd $deploy_directory/temp
x=`grep -o '"http://[^"]*"' $main_file | grep 7000`
if [[ $x ]]; then
    old_url=`echo $x | cut -f2 -d '"'`
    sed -i "s,$old_url,$new_url,g" $main_file
    changed_url=`grep -o '"https://[^"]*"' $main_file`
else
    old_url=`grep -o '"https://[^"]*"' $main_file | grep -v phonebookservice`
    old_url=`echo $old_url | cut -f2 -d '"'`
    sed -i "s,$old_url,$new_url,g" $main_file
    changed_url=`grep -o '"https://[^"]*"' $main_file`
fi
jar -cvf PhoneBookWebApp.war * &> /dev/null
cp PhoneBookWebApp.war $deploy_directory

cd $deploy_directory

echo "Deploying Cloudformation stack with UI application...."
x=$((aws cloudformation describe-stacks --stack-name pge-directory) 2>&1)
if [[ $x = *"ValidationError"* ]]; then
    sh ./updated_wrapper.sh
else
    aws cloudformation delete-stack --stack-name pge-directory
    aws cloudformation wait stack-delete-complete --stack-name pge-directory
    sh ./updated_wrapper.sh
fi
echo "The deployment is complete. Here is the new URL"
x=$((aws cloudformation describe-stacks --stack-name pge-directory --query 'Stacks[0].Outputs[?OutputKey==`URL`].OutputValue' --output text) 2>&1)
echo $x

