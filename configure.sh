#!/bin/bash
set -e -x
yum update -y
yum erase -y ntp*
yum install -y chrony
service chronyd start
chkconfig chronyd on
chronyc sources -v
chronyc tracking

echo "root soft nofile 65536" >> /etc/security/limits.conf
echo "root hard nofile 65536" >> /etc/security/limits.conf
echo "* soft nofile 65536" >> /etc/security/limits.conf
echo "* hard nofile 65536" >> /etc/security/limits.conf

# Install dependencies
yum install -y git tomcat tomcat-webapps tomcat-admin-webapps


instance_id=`curl http://169.254.169.254/latest/meta-data/instance-id`
public_ip=`curl http://169.254.169.254/latest/meta-data/public-ipv4`
echo "Public ip of instance created is $public_ip"
service rsyslog restart
#Tomcat related settings

role='<role rolename="admin"/>'
user='<user username="admin" password="welcome1" roles="manager-gui,manager-script,manager-jmx,manager-status,admin-gui,admin-script"/>'
y=`cat /etc/tomcat/tomcat-users.xml | wc -l`

sed -i "$[ $y ]i\\$user\\" /etc/tomcat/tomcat-users.xml
sed -i "$[ $y ]i\\$role\\" /etc/tomcat/tomcat-users.xml

service tomcat start
