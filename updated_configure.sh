#!/bin/bash
set -e -x
#yum update -y
yum erase -y ntp*
yum install -y chrony
service chronyd start
chkconfig chronyd on
chronyc sources -v
chronyc tracking

echo "root soft nofile 65536" >> /etc/security/limits.conf
echo "root hard nofile 65536" >> /etc/security/limits.conf
echo "* soft nofile 65536" >> /etc/security/limits.conf
echo "* hard nofile 65536" >> /etc/security/limits.conf

# Install dependencies
yum install -y git tomcat tomcat-webapps tomcat-admin-webapps

deploy_application(){

  echo "Fetching the war file from S3"
  cd /tmp
  aws s3api get-object --bucket my-bucket-0909090 --key PhoneBookWebApp.war PhoneBookWebApp.war
  ip_address=$1
  echo "-------------DEPLOYING APP on $ip_address--------------"

  echo "Tomcat is available now"
  echo "Deploying application to tomcat"

  z=`curl -u admin:welcome1 -T PhoneBookWebApp.war http://$ip_address:8080/manager/text/deploy?path=/phonebook`

  if [[ $z = *"FAIL"* ]]; then
    echo "Failed to deploy app successfully on server $ip_address"
  else
    echo "Successfully deployed application on server $ip_address"
    rm PhoneBookWebApp.war
  fi

}

instance_id=`curl http://169.254.169.254/latest/meta-data/instance-id`
public_ip=`curl http://169.254.169.254/latest/meta-data/public-ipv4`
local_ip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
echo "Public ip of instance created is $public_ip"
service rsyslog restart

#Tomcat related settings
role='<role rolename="admin"/>'
user='<user username="admin" password="welcome1" roles="manager-gui,manager-script,manager-jmx,manager-status,admin-gui,admin-script"/>'
y=`cat /etc/tomcat/tomcat-users.xml | wc -l`

sed -i "$[ $y ]i\\$user\\" /etc/tomcat/tomcat-users.xml
sed -i "$[ $y ]i\\$role\\" /etc/tomcat/tomcat-users.xml

systemctl enable tomcat.service
systemctl start tomcat.service

sleep 10

deploy_application $local_ip

echo "
#!/bin/bash

while : ; do
true
done" > /tmp/cpu_stress.sh

systemctl restart tomcat.service
